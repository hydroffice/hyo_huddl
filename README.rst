HydrOffice Huddl
================

.. image:: https://www.hydroffice.org/static/myhuddl/img/logo.png
    :alt: logo


* Code: `GitHub repo <https://github.com/hydroffice/hyo_huddl>`_
* Project page: `url <https://www.hydroffice.org/huddl/main>`_
* Download page: `url <https://bitbucket.org/hydroffice/hyo_huddl/downloads/>`_
* License: LGPLv3 or IA license (See `Dual license <https://www.hydroffice.org/license/>`_)

|

General Info
------------

.. image:: https://api.codacy.com/project/badge/Grade/56d16b63655544b5827740b6473cc4d3
    :target: https://www.codacy.com/app/hydroffice/hyo_huddl/dashboard
    :alt: codacy

HydrOffice is a research development environment for ocean mapping. It provides a collection of hydro-packages, 
each of them dealing with a specific issue of the field.
The main goal is to speed up both algorithms testing and research-2-operation.

HydrOffice Huddl is a tool set to manage hydrographic raw data formats. It has 3 main components:

* Huddl, the Hydrographic Universal Data Description Language, a XML dialect to describe the data formats.
* Huddler, a compiler that uses Huddl Format Descriptions to generate code to access the raw data.
* RawData, the library that actually provides access to the data in various programming languages.


Credits
-------

Authors
~~~~~~~

This code is written and maintained by:

- `Giuseppe Masetti <mailto:gmasetti@ccom.unh.edu>`_


Contributors
~~~~~~~~~~~~

The following wonderful people contributed directly or indirectly to this project:

- `Brian Calder <mailto:brc@ccom.unh.edu>`_

Please add yourself here alphabetically when you submit your first pull request.

